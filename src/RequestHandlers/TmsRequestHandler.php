<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 06/07/2018, 12:49 PM
 */

namespace Ph\Internal\Helpers\RequestHandlers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class TmsRequestHandler
 *
 * @package Ph\Internal\Helpers\RequestHandlers
 */
class TmsRequestHandler
{
    private $userIdentity = '';
    private $password = '';
    private $error = '';
    private $token = '';
    private $timezone;

    private static $baseUrl = 'https://api.lel.asia/v1/';

    private $logger;

    /**
     * TmsRequestHandler constructor.
     */
    public function __construct()
    {
        $this->userIdentity = config('auth.tms.username');
        $this->password     = config('auth.tms.password');
        $this->timezone     = new \DateTimeZone('Asia/Manila');
        $this->logger       = app(LoggerInterface::class);
    }

    /**
     * @return TmsRequestHandler
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getInstance(): TmsRequestHandler
    {
        $token = Cache::get(self::cacheKey());
        $tmsHandler = new self;

        if ($token) {
            $tmsHandler->setToken($token);
        }

        if ((!$token || empty($token)) && $tmsHandler->handle()) {
            $token = $tmsHandler->getToken();
            try {
                Cache::put(self::cacheKey(), $token, 59);
            } catch (\Exception $exception) { }
        }

        return $tmsHandler;
    }

    /**
     * @return string
     */
    private static function cacheKey(): string
    {
        return md5('tms_token' . self::$baseUrl);
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * Attempt login
     *
     * @return bool
     * @throws GuzzleException
     */
    public function handle(): bool
    {
        $client = $this->getHttpClient();

        try {
            $loginResponse = $client->send(
                new Request(
                    'POST',
                    self::$baseUrl . 'authenticate',
                    ['Content-Type' => 'application/json'],
                    json_encode(
                        [
                            'username' => $this->userIdentity,
                            'password' => $this->password
                        ]
                    )
                )
            );

            $response = json_decode((string) $loginResponse->getBody(), true);
            $this->token = $response['token'] ?? '';
            return true;
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $this->logger->error($exception->getTraceAsString());
        }

        $this->error = 'Unexpected error. Source: TMS';
        return false;
    }

    /**
     * Get Auth error if any
     *
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return Client
     */
    private function getHttpClient(): Client
    {
        return app('helper.http.client');
    }

    /**
     * @return array
     */
    private function getRequestOptions(): array
    {
        return [
            RequestOptions::TIMEOUT => 30,
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
        ];
    }

    /**
     * @param string $trackingNumber
     * @param bool   $asPackageNumber
     *
     * @return array
     */
    public function getPackageItemsByTrackingNumber(string $trackingNumber, $asPackageNumber = false): array
    {
        $package = $this->packageFromCache($trackingNumber);
        if ([] !== $package) {
            return $package;
        }

        $searchUrl = self::$baseUrl . 'packages?lastMileTrackingNumber=' . $trackingNumber;
        if ($asPackageNumber) {
            $searchUrl = str_replace('lastMileTrackingNumber', 'platformPackageId', $searchUrl);
        }

        $options = $this->getRequestOptions();
        $client = $this->getHttpClient();

        $response = (string) $client->get($searchUrl, $options)->getBody();
        $decoded = json_decode($response, true)['data'] ?? [];
        $packageId = $decoded[0]['id'] ?? null;

        $body = (string) $client->get(self::$baseUrl . 'packages/' . $packageId, $options)->getBody();
        $package = json_decode($body, true)['data'] ?? [];

        $spProvider     = [];
        $allProviders   = [];

        if (isset($package['journey'])) {
            $providerIds = collect($package['journey'])->unique('shippingProviderId')
                ->pluck('shippingProviderId')->values();

            $postPromises = [];
            $postPromises[] = $client->requestAsync(
                'GET',
                self::$baseUrl . 'shipping-providers/' . $package['journey']['lastMile']['shippingProviderId'] ?? null,
                $options
            )->then(
                function (ResponseInterface $response) {
                    $body = (string) $response->getBody();
                    return [ 'data' => json_decode($body, true)['data'] ?? [], 'key' => 'last_mile_provider' ];
                }
            );

            foreach ($providerIds as $shippingProvider) {
                $spUrl = self::$baseUrl . 'shipping-providers/' . $shippingProvider;
                $postPromises[] = $client->requestAsync('GET', $spUrl, $options)->then(
                    function (ResponseInterface $response) {
                        $body = (string) $response->getBody();
                        return [ 'data' => json_decode($body, true)['data'] ?? [], 'key' => 'shipping_provider' ];
                    }
                );
            }

            (new EachPromise($postPromises, [
                'concurrency'   => count($postPromises),
                'fulfilled'     => function ($result) use (&$allProviders, &$spProvider) {
                    if ($result['key'] === 'shipping_provider') {
                        $allProviders[] = $result['data']['name'] ?? null;
                    } elseif ($result['key'] === 'last_mile_provider') {
                        $spProvider = $result['data'];
                    }
                }
            ]))->promise()->wait();
        }

        $toReturn = array_map(
            function ($item) use ($package, $trackingNumber, $spProvider, $allProviders) {
                return (object) [
                    'package_id'        => $package['id'] ?? '',
                    'sku'               => $item['sku'] ?? '',
                    'sku_description'   => $item['name'] ?? '',
                    'tracking_number'   => $package['journey']['lastMile']['trackingNumber'] ?? $trackingNumber,
                    'package_number'    => $package['platformPackageId'],
                    'sp_provider'       => str_replace(' PH', '', $spProvider['name'] ?? ''),
                    'current_status'    => $package['platformStatus'] ?? '',
                    'payment_method'    => $package['paymentType'] ?? '',
                    'quantity'          => $item['quantity'] ?? 1,
                    'price'             => $item['price'] ?? 0,
                    'totalPrice'        => $package['totalPrice'],
                    'shipper'           => $package['shipper'],
                    'destination'       => $package['customer']['address']['addressId'] ?: '',
                    'packageType'      => strtoupper($package['packageType'] ?? ''),
                    'isMp'              => strtoupper($package['shippingType'] ?? '') === 'MARKETPLACE',
                    'allProviders'      => $allProviders,
                ];
            },
            $package['items'] ?? []
        );

        if ($toReturn === [] && !$asPackageNumber) {
            $toReturn = $this->getPackageItemsByTrackingNumber(strtolower($trackingNumber), true);
        }

        // Only Cache when there's data to return
        if ([] !== $toReturn) {
            $this->cachePackage($trackingNumber, $toReturn);
        }

        return $toReturn;
    }

    /**
     * @param string $identifier
     * @param array $package
     * @param int $time
     */
    private function cachePackage($identifier, array $package, int $time = 15)
    {
        if ($package === []) {
            return;
        }

        // Cache Package Response for 60 Minutes
        Cache::put($identifier, json_encode($package), $time);
    }

    /**
     * @param $identifier
     * @return array
     */
    private function packageFromCache($identifier): array
    {
        $packageString = Cache::get($identifier);
        if ($packageString) {
            $package = json_decode($packageString, true);
            if (JSON_ERROR_NONE !== json_last_error()) {
                return [];
            }

            return array_map(function ($item) {
                return (object) $item;
            }, $package);
        }

        return [];
    }

    /**
     * @param string $identifier
     *
     * @param bool $asTrackingNumber
     * @return array
     */
    public function getStatusAndDetails(string $identifier, $asTrackingNumber = false): array
    {
        $client     = $this->getHttpClient();
        $options    = $this->getRequestOptions();

        if ($asTrackingNumber) {
            $response = $client->get(self::$baseUrl . 'packages?lastMileTrackingNumber=' . $identifier, $options);
            $parsed = json_decode((string) $response->getBody(), true)['data'][0] ?? [];

            $identifier = $parsed['id'] ?? null;
        }

        $prUrl          = self::$baseUrl . 'pickup-requests?platformPackageId=' . $identifier;
        $promises       = [];
        $packageData    = [];
        $packageId      = $identifier;

        // Get Pickup Request Statuses
        $promises[] = $client->requestAsync('GET', $prUrl, $options)->then(
            function (ResponseInterface $response) use (&$packageId) {
                $body       = (string) $response->getBody();
                $decoded    = json_decode($body, true)['data'] ?? [];

                return [ 'pr_status' => $decoded ];
            }
        );

        $packageInfoUrl = str_replace('v1', 'v2', self::$baseUrl) . 'packages/' . $packageId;
        // Get Package Info (v2)
        $promises[] = $client->requestAsync('GET', $packageInfoUrl, $options)->then(
            function (ResponseInterface $response) {
                $body       = (string) $response->getBody();
                $decoded    = json_decode($body, true)['data'] ?? [];

                return [ 'package_info' => $decoded ];
            }
        );

        // Get Package Info (v1)
        $promises[] = $client->requestAsync('GET', self::$baseUrl . 'packages/' . $packageId, $options)->then(
            function (ResponseInterface $response) {
                $body       = (string) $response->getBody();
                $decoded    = json_decode($body, true)['data'] ?? [];

                return [ 'package_info_v1' => $decoded ];
            }
        );

        (new EachPromise($promises, [
            'concurrency'   => 2,
            'fulfilled'     => function ($result) use (&$packageData) {
                $packageData = array_merge($packageData, $result);
            }
        ]))->promise()->wait();

        $packageData['package_info'] = array_merge($packageData['package_info_v1'], $packageData['package_info']);
        unset($packageData['package_info_v1']);

        $postPromises = [];
        foreach ($packageData['pr_status'] ?? [] as $pickupRequest) {
            $url =  self::$baseUrl . 'pickup-requests/' . $pickupRequest['id'] . '/statuses';

            $postPromises[] = $client->requestAsync('GET', $url, $options)->then(
                function (ResponseInterface $response) use ($pickupRequest) {
                    $body = (string) $response->getBody();
                    return [ 'data' => json_decode($body, true)['data'] ?? [], 'key' => $pickupRequest['id'] ];
                }
            );

            $spUrl = self::$baseUrl . 'shipping-providers/' . $pickupRequest['shippingProviderId'];
            $postPromises[] = $client->requestAsync('GET', $spUrl, $options)->then(
                function (ResponseInterface $response) {
                    $body = (string) $response->getBody();
                    return [ 'data' => json_decode($body, true)['data'] ?? [], 'key' => 'shipping_provider' ];
                }
            );
        }

        (new EachPromise($postPromises, [
            'concurrency'   => count($postPromises),
            'fulfilled'     => function ($result) use (&$packageData) {
                if ($result['key'] === 'package_info') {
                    $packageData[$result['key']] = $result['data'];
                } elseif ($result['key'] === 'shipping_provider') {
                    $packageData['shipping_providers'][$result['data']['id']] = $result['data'];
                } elseif ($result['key'] === 'platform_status') {
                    $key = array_search($result['row'], $packageData['platform_statuses'], false);
                    $packageData['platform_statuses'][$key]['pushedData'] = $result['data'];
                } else {
                    $packageData['pr_status_histories'][$result['key']] = $result['data'];
                }
            }
        ]))->promise()->wait();

        if (empty($packageData['package_info']) && !$asTrackingNumber) {
            return $this->getStatusAndDetails($identifier, true);
        }

        return $packageData;
    }

    /**
     * @param array $packageNumbers
     *
     * @return array
     */
    public function multiPackageDetails(array $packageNumbers): array
    {
        $results = [];
        collect($packageNumbers)->each(
            function ($packageNumber) use (&$results) {
                if (!empty($packageNumber)) {
                    $results[] = $this->getStatusAndDetails($packageNumber);
                }
            }
        );

        return $results;
    }

    /**
     * @return array
     */
    public function getShippingProviders(): array
    {
        $url = self::$baseUrl . 'shipping-providers?address_id=R443174&page=1&per_page=1000';

        $options    = $this->getRequestOptions();
        $client     = $this->getHttpClient();

        $resp = (string) $client->get($url, $options)->getBody();
        $decoded = json_decode($resp, true)['data'] ?? [];

        return array_map(
            function ($row) {
                return [
                    'id' => $row['id'],
                    'name' => $row['name'],
                ];
            },
            $decoded
        );
    }

    /**
     * @param string $id
     * @return array
     */
    public function getBranches(string $id): array
    {
        $url = self::$baseUrl . 'shipping-providers/{id}/branches?page=1&per_page=10000';
        $url = str_replace('{id}', $id, $url);

        $options = $this->getRequestOptions();
        $client = $this->getHttpClient();

        $resp = (string) $client->get($url, $options)->getBody();
        $decoded = json_decode($resp, true)['data'] ?? [];

        return array_map(
            function ($row) {
                return [
                    'id'        => $row['id'],
                    'name'      => $row['name'],
                    'portCode'  => $row['portCode'],
                    'region'    => $row['region'],
                ];
            },
            $decoded
        );
    }

    /**
     * @param string $id
     */
    public function deleteBranches(string $id): void
    {
        // To Skip
        $skip = [
            '7d75d13c-e4be-4adb-80ff-80959cca998a', // AIRSPEED
            '22d386e7-bceb-4131-be10-11024b9538f3', // 2GO
            'b69c8158-aa7f-40e2-ac91-b8abc32ca9c6', // Cargo King
            '2567af49-4f44-4882-b003-03dedc9b1a4b', // LBC
            'c8b9dc16-0657-48a7-9816-4d60a9571175', // LEX
            '4014bd37-ca47-40cb-bc5d-60fffb6da5f4', // NJV,
            'dd78b3dd-c13e-48b9-9fc1-0feb3aaa817f', // XDE
            '7d48687e-7ae3-4955-8190-8b3e34ed88b9', // JRMT
        ];

        if (!in_array($id, $skip, true)) {
            while (count($this->getBranches($id))) {
                $branches = $this->getBranches($id);
                echo "Got " . count($branches) . " branches for $id\n";

                foreach ($branches as $branch) {
                    $url = self::$baseUrl . 'shipping-providers/{id}/branches/' . $branch['id'];
                    $url = str_replace('{id}', $id, $url);

                    $options = $this->getRequestOptions();
                    $client = $this->getHttpClient();

                    $client->delete($url, $options);
                }
            }
        } else {
            echo "Skipped $id \n";
        }
    }
}
