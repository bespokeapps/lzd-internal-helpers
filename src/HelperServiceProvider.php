<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 06/07/2018, 12:52 PM
 */

namespace Ph\Internal\Helpers;

use function GuzzleHttp\choose_handler;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\ServiceProvider;
use Ph\Internal\Contracts\Middleware\Loggers\IHttpLogFormatter;
use Ph\Internal\Helpers\Formatters\HttpLogFormatter;
use Ph\Internal\Helpers\Middleware\ContextSetterMiddleware;
use Ph\Internal\Helpers\Middleware\Loggers\ApiLogger;
use Ph\Internal\Helpers\Middleware\OutwardTrafficLogger;
use Ph\Internal\Helpers\Utils\Labels;

/**
 * Class HelperServiceProvider
 *
 * @package Ph\Internal\Helpers
 */
class HelperServiceProvider extends ServiceProvider
{
    private $options = [
        'log_outgoing_requests' => true,
    ];

    /**
     * HelperServiceProvider constructor.
     * @param $app
     * @param array $options
     */
    public function __construct($app, array $options = [])
    {
        parent::__construct($app);

        $this->options = array_merge($this->options, $options);
    }

    /**
     * Register
     */
    public function register()
    {
        $this->app->singleton(Labels::HTTP_CLIENT, function () {
            $stack = new HandlerStack(choose_handler());

            // Only Log when specifically requested by the client
            if ($this->options['log_outgoing_requests'] ?? false) {
                $stack->push(app(OutwardTrafficLogger::class), OutwardTrafficLogger::MIDDLEWARE_NAME);
            }

            $stack->push(Middleware::cookies(), 'cookies');
            $stack->push(Middleware::redirect(), 'redirect_handler');
            $stack->push(Middleware::prepareBody(), 'prepare_body');

            $client = new Client(
                [
                    'handler'                       => $stack,
                    RequestOptions::ALLOW_REDIRECTS => true,
                    RequestOptions::VERIFY          => false,
                    RequestOptions::CONNECT_TIMEOUT => 10,
                    RequestOptions::TIMEOUT         => 600,
                ]
            );

            return $client;
        });

        $this->app->singleton(IHttpLogFormatter::class, HttpLogFormatter::class);

        // Provide Middleware to enable Logging
        $this->app->routeMiddleware([
            Labels::API_LOGGER  => ApiLogger::class,
            Labels::ROUTE_LABEL => ContextSetterMiddleware::class
        ]);
    }
}
