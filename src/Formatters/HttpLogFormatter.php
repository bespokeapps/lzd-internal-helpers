<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 19/07/2018, 5:16 PM
 */

namespace Ph\Internal\Helpers\Formatters;

use Ph\Internal\Contracts\Middleware\Loggers\IHttpLogFormatter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HttpLogFormatter
 *
 * @package Ph\Internal\Helpers\Formatters
 */
class HttpLogFormatter implements IHttpLogFormatter
{

    protected $timer;

    /**
     * @param Request $request
     * @param Response $response
     * @return array
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function prepare(Request $request, Response $response): array
    {
        $timestamp = (new \DateTime())->format(DATE_RFC3339);

        return [
            '@timestamp'=> $timestamp,
            'datetime'  => $timestamp,
            'request'   => $this->toString($request),
            'response'  => $this->toString($response),
        ];
    }

    /**
     * @param Request|Response $message
     *
     * @return string
     */
    private function toString($message)
    {
        if ($message instanceof Request) {
            $msg = trim($message->getMethod() . ' '
                    . $message->getUri())
                . ' HTTP/' . $message->getProtocolVersion();
            if (!$message->headers->has('host')) {
                $msg .= "\r\nHost: " . $message->getHost();
            }
        } elseif ($message instanceof Response) {
            $msg = 'HTTP/' . $message->getProtocolVersion() . ' ' . $message->getStatusCode() . ' ';
        } else {
            $msg = 'Error!';
        }

        foreach ($message->headers->all() as $name => $values) {
            $msg .= "\r\n{$name}: " . implode(', ', $values);
        }

        return "{$msg}\r\n\r\n" . $message->getContent();
    }
}
