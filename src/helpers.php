<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 06/07/2018, 12:40 PM
 */

use Illuminate\Container\Container;

if (! function_exists('app')) {
    /**
     * Get the available container instance.
     *
     * @param  string  $make
     * @return mixed|
     */
    function app($make = null)
    {
        if (is_null($make)) {
            return Container::getInstance();
        }

        return Container::getInstance()->make($make);
    }
}

if(!function_exists('config_path'))
{
    /**
     * Return the path to config files
     * @param null $path
     * @return string
     */
    function config_path($path=null)
    {
        return app()->getConfigurationPath(rtrim($path, ".php"));
    }
}
if(!function_exists('public_path'))
{
    /**
     * Return the path to public dir
     * @param null $path
     * @return string
     */
    function public_path($path=null)
    {
        return rtrim(app()->basePath('public/'.$path), '/');
    }
}
if(!function_exists('storage_path'))
{
    /**
     * Return the path to storage dir
     * @param null $path
     * @return string
     */
    function storage_path($path=null)
    {
        return app()->storagePath($path);
    }
}
if (! function_exists('config')) {
    /**
     * Get / set the specified configuration value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string  $key
     * @param  mixed  $default
     * @return mixed
     */
    function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('config');
        }

        if (is_array($key)) {
            return app('config')->set($key);
        }

        return app('config')->get($key, $default);
    }
}
