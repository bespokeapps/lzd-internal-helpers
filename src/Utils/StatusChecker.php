<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 06/07/2018, 1:31 PM
 */

namespace Ph\Internal\Helpers\Utils;

use GuzzleHttp\Exception\GuzzleException;
use Ph\Internal\Contracts\IBot;

/**
 * Class StatusChecker
 *
 * @package Ph\Internal\Helpers\Utils
 */
class StatusChecker
{

    /**
     * @return array
     */
    public static function checkSystemStatus(): array
    {
        $status = [
            'read_database_okay'    => true,
            'write_database_okay'   => true,
            'redis_okay'            => true,
            'tms_conn_okay'         => true,
            'elastic_search'        => true,
        ];

        $queues     = [ 'active_queues' => [], ];
        $messages   = [];

        try {
            $client = app(\Elastica\Client::class);
            if (!$client->hasConnection()) {
                $client->connect();
            }

            $status['elastic_search'] = $client->hasConnection();
        } catch (\Exception $exception) {
            $status['elastic_search'] = false;
            $messages[] = sprintf('Cannot connect to ElasticSearch: %s.', $exception->getMessage());
        }

        try {
            (new \Predis\Client(config('database.redis.default')))->connect();
        } catch (\Exception $exception) {
            $status['redis_okay'] = false;
            $messages[] = sprintf('Cannot connect to redis: %s.', $exception->getMessage());
        }

        try {
            $client = new \Predis\Client(config('database.redis.default'));
            $list = $client->keys('queues*');

            foreach ($list as $queue) {
                $queues['active_queues'][$queue] = 0;

                try {
                    $queues['active_queues'][$queue] = $client->llen($queue);
                } catch (\Exception $exception) {
                }
            }

            $queues['total_jobs_in_queue'] = array_reduce(
                array_values($queues['active_queues']),
                function ($a, $b) {
                    return $a + $b;
                },
                0
            );
        } catch (\Exception $e) {

        }

        try {
            /** @noinspection SqlNoDataSourceInspection */
            /** @noinspection SqlDialectInspection */
            \DB::select('select * from package_db.final_package_dimensions limit 1');
        } catch (\PDOException $exception) {
            $status['read_database_okay'] = false;
            $messages[] = sprintf('Cannot connect to database: %s.', $exception->getMessage());
        }

        try {
            /** @noinspection SqlNoDataSourceInspection */
            /** @noinspection SqlDialectInspection */
            \DB::selectFromWriteConnection('select * from package_db.final_package_dimensions limit 1');
        } catch (\PDOException $exception) {
            $status['write_database_okay'] = false;
            $messages[] = sprintf('Cannot connect to database: %s.', $exception->getMessage());
        }

        try {
            (new PackageHelper)->getPackage('1234');
        } catch (GuzzleException $exception) {
            $status['tms_conn_okay'] = false;
            $messages[] = sprintf('Cannot connect to TMS API: %s', $exception->getMessage());
        }

        $allSystemsUp = array_reduce(array_values($status), function($a, $b) {
            return $a && $b;
        }, true);

        if (str_contains(gethostname(), 'snp')) {
            $queueWorkers = [
                'dim.weight.sender' => 0,
                'cb.dim.weight'     => 0,
                'mass.upload'       => 0,
                'reverse.billing'   => 0,
                'redis --sleep=60'  => 0,
            ];

            exec('ps -f -u dapo', $output);
            foreach ($output as $line) {
                foreach ($queueWorkers as $key => $value) {
                    if (str_contains($line, $key)) {
                        $queueWorkers[$key] += 1;
                    }
                }
            }

            $queueWorkers['default'] = $queueWorkers['redis --sleep=60'];
            unset($queueWorkers['redis --sleep=60']);
            $queues['workers'] = $queueWorkers;

            $queues['total_workers'] = array_reduce(
                array_values($queues['workers']),
                function ($a, $b) {
                    return $a + $b;
                },
                0
            );
        }

        // TODO: Check 3PL APIs
        return [
            'allSystemsUp'  => $allSystemsUp,
            'systems_check' => $status,
            'messages'      => $messages,
            'queue_status'  => $queues,
        ];
    }

    /**
     * Notifier
     * @param bool $sendAnyway
     */
    public static function checkDwsHandoverStatus($sendAnyway = false)
    {
        try {
            /** @var IBot $bot */
            $bot = app(IBot::class);

            $query = 'count(rd.tracking_number_adjust) as dws_raw,
                count(fd.tracking_number) as handover_vol,
                count(if(dtl.last_mile_tpl IN (
                        \'LBC\',
                        \'2GO EXPRESS\',
                        \'CARGO KING\',
                        \'AIRSPEED\',
                        \'XIMEX DELIVERY EXPRESS\'),fd.tracking_number,null)) as handover_3pl_w_api_vol,
                count(if(fd.is_sent = 1, fd.tracking_number,null)) as sent_to_3pl_vol';

            $result = app('db')::connection('transport')->query()
                ->selectRaw($query)
                ->from('package_db.dws_raw_data as rd')
                ->join(
                    'package_db.final_package_dimensions as fd',
                    'tracking_number_adjust',
                    'fd.tracking_number'
                )
                ->join(
                    'package_db.tms_package_dtl as dtl',
                    'dtl.package_id',
                    'fd.package_id'
                )
                ->whereRaw('rd.`timestamp` >= now() - interval 14 hour AND rd.`timestamp` < now() - interval 1 hour')
                ->whereRaw('error = 0 and tracking_number_adjust is not null and weight_adjust > 0')
                ->first();

            $message = '';

            $dwsMargin = self::getMargin($result->handover_vol, $result->dws_raw);
            $transMargin = self::getMargin($result->sent_to_3pl_vol, $result->handover_3pl_w_api_vol);

            if ($dwsMargin > 10) {
                $message .= sprintf(
                    '<p>There is a <b>%s</b> margin in records from DWS and records in Final Package Dimensions</p>',
                    $dwsMargin . '%'
                );
            }

            if ($transMargin > 10) {
                $message .= sprintf(
                    '<p><b>%s</b> of DimWeight Data is pending Transmission to 3PLs</p>',
                    $transMargin . '%'
                );
            }

            if (!empty($message) || $sendAnyway) {
                $message .= sprintf(
                    '<hr><ul><li><b>DWS RAW DATA</b>: %s</li><li><b>FINAL PK DIMS</b>: %s</li><li><b>Margin</b>: %s</li><li><b>TRANSMITTABLE</b>: %s</li><li><b>SENT TO 3PL</b>: %s</li><li><b>MARGIN</b>: %s</li></ul>',
                    number_format($result->dws_raw),
                    number_format($result->handover_vol),
                    $dwsMargin . '%',
                    number_format($result->handover_3pl_w_api_vol),
                    number_format($result->sent_to_3pl_vol),
                    $transMargin . '%'
                );

                $converter = app('app.html.markdown');
                $bot->sendRichMessage('DWS Tracker', $converter->convert($message));
            }
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * @param $numerator
     * @param $denominator
     *
     * @return float
     */
    public static function getMargin($numerator, $denominator)
    {
        return round((($denominator - $numerator) / $denominator) * 100, 2);
    }
}
