<?php
declare(strict_types = 1);

/**
 * @author  Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 10/27/2017, 00:04
 * @package App
 */

namespace Ph\Internal\Helpers\Utils;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Collection;
use Ph\Internal\Helpers\RequestHandlers\TmsRequestHandler;

/**
 * Class PackageHelper
 *
 * @package App
 */
class PackageHelper
{
    const POUCHES = ['XS', 'S', 'M', 'L', 'XL'];

    /** @var  Collection */
    private $dataMartPackage;

    private $maxSides;
    private $maxWeight;

    /**
     * PackageHelper constructor.
     */
    public function __construct()
    {
        $this->maxWeight    = config('sizes.max_dimensions.weight');
        $this->maxSides     = config('sizes.max_dimensions.sides');
    }

    /**
     * @param string $size
     *
     * @return bool
     */
    public function isPouch(string $size): bool
    {
        return in_array($size, self::POUCHES, true);
    }

    /**
     * @return bool
     */
    private function isMp(): bool
    {
        return $this->dataMartPackage->first()->isMp;
    }

    /**
     * @param string $identifier
     * @param string $pouchSize
     *
     * @return array|bool
     * @throws GuzzleException
     */
    public function getPackageDimensions(string $identifier, string $pouchSize = 'B')
    {
        $package = $this->getPackage($identifier);
        $itemCount = $package->count();

        if ($itemCount === 0) {
            return false;
        }

        /** @var Connection $connection */
        $connection = app(ConnectionInterface::class);

        // If Dimensions are already saved, return the Saved Dimensions
        $finalDims = $connection->query()
            ->select('*')
            ->from('package_db.final_package_dimensions')
            ->orWhere('package_number', $identifier)
            ->orWhere('tracking_number', $identifier)->first();

        if ($finalDims) {
            return [
                'data' => (object) [
                    'package_weight'    => $finalDims->weight,
                    'package_length'    => $finalDims->length,
                    'package_width'     => $finalDims->width,
                    'package_height'    => $finalDims->height,
                    'pouch_size'        => $finalDims->pouch_size,
                    'package_number'    => $finalDims->package_number,
                    'sp_provider'       => $package->first()->sp_provider ?? $package->first()['sp_provider'],
                ],
                'source'    => $finalDims->source,
                'package'   => $package,
            ];
        }

        $item = $package->get(0);
        $skus = array_unique($package->pluck('sku')->all());

        // Get Manual Measurements First
        $measurement = $this->getManualMeasurement($connection, $item, $itemCount, count($skus));
        if ($measurement && $measurement->package_weight) {
            $measurement->package_number = $item->package_number ?? '';

            $this->adjustSinglePackageDimensionContent($item, $measurement, $itemCount);
            return [ 'data' => $measurement, 'source' => 'manual', 'package' => $package ];
        }

        if (($itemCount === 1 && !empty($item->sku)) || ($itemCount > 1 && count($skus) === 1)) {
            $dimension = $this->getRetailDim($connection, $item);

            if ($dimension && !empty($dimension->package_weight)) {
                $this->adjustSinglePackageDimensionContent($item, $dimension, $itemCount);

                // Check for Max Dimensions
                if ($dimension->package_weight > $this->maxWeight ||
                    (float) $dimension->package_length > $this->maxSides ||
                    (float) $dimension->package_width > $this->maxSides ||
                    (float) $dimension->package_height > $this->maxSides
                ) {
                    $dimension = null;
                } else {
                    return ['data' => $dimension, 'source' => 'oms', 'package' => $package];
                }
            }
        } elseif ($this->isPouch($pouchSize)) {
            // Retrieve Sum of all weights for Items in Package
            $dimension = $this->getRetailPouchDim($connection, $package);

            if ($dimension && !empty($dimension->package_weight)) {
                $dimension->sp_provider = $item->sp_provider ?? '';
                return [ 'data' => $dimension, 'source' => 'oms', 'package' => $package ];
            }
        }

        $dimension = $this->getDwsDimension($connection, $item);
        if ($dimension && $dimension->package_weight) {
            if ($dimension->isTray) {
                $sizes = config('sizes')['S'];

                $dimension->package_length  = $sizes[0];
                $dimension->package_width   = $sizes[1];
                $dimension->package_height  = $sizes[2];
            }

            $dimension->sp_provider = $item->sp_provider;
            return ['data' => $dimension, 'source' => $dimension->source, 'package' => $package ];
        }

        return [];
    }

    /**
     * @param string $identifier
     * @param bool $getCached
     * @return Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPackage(string $identifier, bool $getCached = true): Collection
    {
        if ($this->dataMartPackage !== null && $getCached) {
            return $this->dataMartPackage;
        }

        $tmsHandler = TmsRequestHandler::getInstance();
        $this->dataMartPackage = collect($tmsHandler->getPackageItemsByTrackingNumber($identifier));

        return $this->dataMartPackage;
    }

    /**
     * @param $connection
     * @param $item
     *
     * @return mixed
     */
    private function getRetailDim(Connection $connection, $item)
    {
        $dimension = null;

        if (!$this->isMp()) {
            $query = '(length/10) as package_length, (width/10) as package_width, (height/10) as package_height,
                (unit_gross_weight/1000) as package_weight';

            $dimension = $connection->query()->selectRaw($query)->from('package_db.dim_wmp_item')
                ->where('unit_gross_weight', '>', 0)
                ->where('sku', '=', $item->sku ?? null)->first();

            return $dimension;
        }

        return $dimension;
    }

    /**
     * @param $item
     * @param $dimension
     * @param $itemCount
     */
    private function adjustSinglePackageDimensionContent($item, $dimension, $itemCount): void
    {
        $dimension->sp_provider = $item->sp_provider ?? '';

        // Sort the dims
        /** @var array $tempDims */
        $tempDims = [
            'package_length' => (float)$dimension->package_length,
            'package_width'  => (float)$dimension->package_width,
            'package_height' => (float)$dimension->package_height,
        ];

        asort($tempDims);

        // Get shortest side
        /** @noinspection LoopWhichDoesNotLoopInspection */
        foreach ($tempDims as $attr => $value) {
            $keyName = $attr;
            break;
        }

        // Account for instances where Package Contains Multiple instances of the same SKU
        if (isset($dimension->{$keyName}, $dimension->package_weight, $dimension->sku)) {
            $dimension->{$keyName} *= $itemCount;
            $dimension->package_weight *= $itemCount;
        }
    }

    /**
     * @param $connection
     * @param $package
     *
     * @return mixed
     */
    private function getRetailPouchDim(Connection $connection, Collection $package)
    {
        $dimension = null;

        if (!$this->isMp()) {
            // Switch to Dabao SKU Info
            $query = 'SUM(unit_gross_weight/1000) as package_weight';

            $dimension = $connection->query()->selectRaw($query)->from('package_db.dim_wmp_item')
                ->whereIn('sku', $package->pluck('sku')->all())->first();

            return $dimension;
        }

        return $dimension;
    }

    /**
     * @param Connection $connection
     * @param $item
     * @param int $itemCount
     * @param int $skuCount
     * @return mixed
     */
    private function getManualMeasurement(Connection $connection, $item, int $itemCount, int $skuCount)
    {
        // Check if Manual Measurement Exists
        $measurementQ = $connection->query()
            ->selectRaw(
                implode(
                    ', ',
                    [
                        'sku',
                        'weight_adjusted as package_weight',
                        'height as package_height',
                        'length as package_length',
                        'width as package_width',
                        'pouch_size',
                    ]
                )
            )
            ->from('transport_db.package_manual_update as pmu')
            ->whereIn(
                'pmu.package_number',
                [$item->package_number, $item->tracking_number ?? '']
            )->orderBy('pmu.created_at', 'desc');

        if ($itemCount === 1 && !empty($item->sku) || ($itemCount > 1 && $skuCount === 1)) {
            $measurementQ = $measurementQ->orWhere('pmu.sku', $item->sku);
        }

        return $measurementQ->first();
    }

    /**
     * @param $connection
     * @param $item
     *
     * @return mixed
     */
    private function getDwsDimension(Connection $connection, $item)
    {
        // Get DWS Measurement
        $dwsQuery  = 'min(weight_adjust) as package_weight, min(length) as package_length, min(width) as package_width,
            min(height) as package_height, IF(package_db.dws_raw_data.package_number LIKE "%TRAY%", 1, 0) as isTray,
            package_number_adjust as package_number, IF(location="warehouse", "dws_warehouse", "dws_sort") as source';

        $dimension = $connection->query()->selectRaw($dwsQuery)->from('package_db.dws_raw_data')
            ->whereRaw(
                sprintf(
                    '(package_number_adjust = "%s" OR tracking_number_adjust = "%s")',
                    $item->package_number,
                    $item->tracking_number ?? ''
                )
            )
            ->where('error', 0)
            ->where('weight_adjust', '>', 0)
            ->groupBy('tracking_number_adjust')->first();

        return $dimension;
    }
}
