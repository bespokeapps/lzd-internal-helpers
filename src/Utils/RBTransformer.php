<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 06/07/2018, 1:35 PM
 */

namespace Ph\Internal\Helpers\Utils;

use Ph\Internal\Contracts\ReportFields;

/**
 * Class RBTransformer
 *
 * @package Ph\Internal\Helpers\Utils
 */
class RBTransformer
{
    public function transform($row): array
    {
        return [
            ReportFields::LINE_ID           => 'PH',
            ReportFields::TPL_NAME          => $row->sp_provider,
            ReportFields::WARE_HOUSE        => $row->warehouse ?? $row->wh_id ?? '',
            ReportFields::DELIVERY_STATUS   => $row->package_status ?? '',
            ReportFields::SHIP_DATE         => $row->ship_time,
            ReportFields::DELIVERY_DATE     => $row->deliver_time ?? '',
            ReportFields::FAILED_DATE       => $row->not_delivered_time ?? '',
            ReportFields::INVOICE_NUMBER    => '',
            ReportFields::PACKAGE_NUMBER    => $row->package_number,
            ReportFields::TRACKING_NUMBER   => $row->tracking_number,
            ReportFields::R_TRACKING_NUMBER => '',
            ReportFields::ORDER_NUMBER      => $row->order_number,
            ReportFields::SERVICE_TYPE      => $row->delivery_type ?? '',
            ReportFields::DECLARED_VALUE    => $row->declared_value,
            ReportFields::TO_BE_COLLECTED   => $row->amount_to_be_collected,
            ReportFields::PACKAGE_VOLUME    => $row->package_volume,
            ReportFields::DIM_WEIGHT        => 0, // TODO: Calculate?
            ReportFields::PACKAGE_HEIGHT    => $row->package_height,
            ReportFields::PACKAGE_WIDTH     => $row->package_width,
            ReportFields::PACKAGE_LENGTH    => $row->package_length,
            ReportFields::PACKAGE_WEIGHT    => $row->package_weight,
            ReportFields::CHARGEABLE_WEIGHT => $row->chargeable_weight,
            ReportFields::FREIGHT_CHARGE    => $row->carrying_fee,
            ReportFields::COD_FEE           => $row->cod_fee,
            ReportFields::RTS_FEE           => $row->fd_fee,
            ReportFields::ODZ_SURCHARGES    => $row->odz_fee,
            ReportFields::HANDLING_FEE      => '',
            ReportFields::VALUATION_FEE     => $row->valuation_fee,
            ReportFields::PORTERAGE_FEE     => 0,
            ReportFields::PACKAGE_TYPE      => $row->package_type,
            ReportFields::PACKAGE_COUNT     => $row->item_count,
            ReportFields::VAT               => $row->vat,
            ReportFields::TOTAL             => $row->total,
            ReportFields::PAYMENT_METHOD    => $row->payment_method,
            ReportFields::REGION            => $row->region ?? '',
            ReportFields::AREA              => $row->province ?? '',
            ReportFields::CITY              => $row->city ?? '',
            ReportFields::BARANGAY          => $row->barangay ?? '',
            ReportFields::DIM_WEIGHT_REF    => $row->dimweight_ref ?? '',
            ReportFields::FLD_CHANNEL       => $row->channel ?? '',
            ReportFields::FLD_ORDER_TYPE    => $row->parcel_type,
            ReportFields::FULFILMENT_TYPE   => $row->delivery_type ?? '',
            ReportFields::FLD_TARGET_SHEET  => $row->target_sheet,
            ReportFields::FLD_REMARKS       => $row->remarks,
            ReportFields::SKU               => $row->sku,
            ReportFields::SKU_DESCRIPTION   => $row->sku_description,
        ];
    }
}
