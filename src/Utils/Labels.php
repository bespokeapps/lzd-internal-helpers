<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 19/07/2018, 5:33 PM
 */

namespace Ph\Internal\Helpers\Utils;

/**
 * Class Labels
 *
 * @package Ph\Internal\Helpers\Utils
 */
final class Labels
{
    const HTTP_CLIENT       = 'helper.http.client';
    const API_LOGGER        = 'lzd.api.logger';
    const ROUTE_LABEL       = 'lzd.route.label';

    const CONTEXT_UNKNOWN   = 'unknown';
}
