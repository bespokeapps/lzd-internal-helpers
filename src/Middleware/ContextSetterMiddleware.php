<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 19/07/2018, 5:31 PM
 */

namespace Ph\Internal\Helpers\Middleware;

use Illuminate\Http\Request;

/**
 * Class ContextSetterMiddleware
 *
 * @package Ph\Internal\Helpers\Middleware
 */
class ContextSetterMiddleware
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @param string|null $label
     * @return mixed
     */
    public function handle(Request $request, \Closure $next, string $label = null)
    {
        $request->attributes->set('requestType', $label ?? 'unknown');
        return $next($request);
    }
}
