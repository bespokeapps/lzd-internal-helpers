<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 19/07/2018, 5:26 PM
 */

namespace Ph\Internal\Helpers\Middleware\Loggers;

use Ph\Internal\Contracts\Middleware\Loggers\IHttpLogFormatter;
use Illuminate\Http\Request;
use Ph\Internal\Helpers\Utils\Labels;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiLogger
 *
 * @package Ph\Internal\Helpers\Middleware\Loggers
 */
class ApiLogger
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var IHttpLogFormatter
     */
    protected $logFormatter;

    /**
     * ApiLogger constructor.
     *
     * @param LoggerInterface         $logger
     * @param IHttpLogFormatter $httpLogFormatter
     */
    public function __construct(LoggerInterface $logger, IHttpLogFormatter $httpLogFormatter)
    {
        $this->logFormatter = $httpLogFormatter;
        $this->logger       = $logger;
    }

    /**
     * @param Request  $request
     * @param \Closure $next
     *
     * @return Response
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function handle(Request $request, \Closure $next): Response
    {
        $start = microtime(true);

        /** @var Response $response */
        $response = $next($request);
        $requestType = $request->attributes->get('requestType') ?? Labels::CONTEXT_UNKNOWN;

        $extras             = $this->logFormatter->prepare($request, $response);
        $extras['context']  = $requestType;
        $extras['ttl']      = microtime(true) - $start;

        $this->logger->info('Incoming request and response on fulfilled.', $extras);

        return $response;
    }
}
