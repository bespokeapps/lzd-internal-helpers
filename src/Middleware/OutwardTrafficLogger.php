<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 19/07/2018, 5:41 PM
 */

namespace Ph\Internal\Helpers\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Debug\Exception\FlattenException;

/**
 * Class OutwardTrafficLogger
 *
 * @package Ph\Internal\Helpers\Middleware
 */
class OutwardTrafficLogger
{
    const MIDDLEWARE_NAME = 'outward_traffic_logger';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * TrafficLogger constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param callable $handler
     *
     * @return callable
     */
    public function __invoke(callable $handler): callable
    {
        return function (RequestInterface $request, array $options) use ($handler) {
            return $handler($request, $options)->then(
                function ($response) use ($request) {
                    $this->logTraffic('Outgoing Request Fulfilled', $request, $response);

                    return $response;
                },
                function (\Exception $reason) use ($request) {
                    $this->logTraffic('Outgoing Request Rejected', $request, null, $reason);

                    return \GuzzleHttp\Promise\rejection_for($reason);
                }
            );
        };
    }

    /** @noinspection MoreThanThreeArgumentsInspection */
    /**
     * @param string $message
     * @param RequestInterface $request
     * @param ResponseInterface|null $response
     * @param $reason
     */
    protected function logTraffic(
        string $message,
        RequestInterface $request,
        ResponseInterface $response = null,
        $reason = null
    ): void {
        $payload = [
            'request' => \GuzzleHttp\Psr7\str($request),
            'exception' => $reason instanceof \Exception ? FlattenException::create($reason)->toArray() : null,
            'context' => 'outgoing.http'
        ];

        if (null !== $response) {
            $payload['response'] = \GuzzleHttp\Psr7\str($response);
        }

        $this->logger->log($reason ? 'warning' : 'info', $message, $payload);
    }
}
